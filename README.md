##  DEPRECATED
Use the [client-auth](go.getsip.ethz.ch/client-auth) package to use in a Go project (same interface as the 73f5ea133ca5074a41a73e9b60e5f13326c49590) or [cli-auth2](https://gitlab.ethz.ch/vseth/0403-isg/cli/cli-auth2) for a standalone cli-auth.

---

# cli-auth

Cli-Auth handles keycloak authentication for clis. It can be used as binary or go package.

## Usage

### As a Go Package

The binary wrapper in `main.go` gives a good example on how to use the package.

> Note: Because this is a protected git repository you need to use a workaround to add the package to your project. We suggest you clone the repository in your Makefile to the `vendor` directory and then overwrite the path in the `go.mod` file. You can use [peoplectl](https://gitlab.ethz.ch/vseth/0403-isg/cli/peoplectl) as an example.

### As a Binary

The binary takes takes the following flags
- necessary:
    - `client_id` keycloak client requesting the authentication
    - `scope` comma separated list of scopes
    - `ports` comma separated list of ports for the callback, the client will use the first available port an listen to `http://localhost:<port>/callback`
- optional:
    - `realm` realm for the request, otherwise we default to `VSETH`
    - `server_domain` custom keycloak server domain, otherwise we default to `https://auth.vseth.ethz.ch`, the `https://` prefix is added, if not present
    - `insecure` use http instead of https (will only work on localhost)

The output is a JSON object.
> Don't evaluate the access token, it is only meant to be forwarded!

If an error occurs:
```json
{
    "error" : <error message>
}
```
Otherwise:
```json
{
    "access_token" : <raw access token>,
    "id_token" : <raw id token>,
    "claims" : {
        <claim name as string> : <value>
    }
}
```

Example: \
`cli-auth -client_id vseth-servis-cli -scope profile -ports 58632,31007,29101,26682,14264,50197,19130,42768,20230,52254`


### As a Service Account
If you use this in an automated system, you usually cannot spawn a browser to authenticate the
CLI. For that you can us service accounts and password authentication. In that mode the binary
takes takes the following flags
- necessary:
    - `service_account_user` The username of the service account
    - `service_account_pw` The password of the service account
- optional:
    - `scope` comma separated list of scopes. If none are provided all are requested *Note: Keycloak
    does not seem to support this at the time of writing this.*
    - `realm` realm for the request, otherwise we default to `VSETH`
    - `server_domain` custom keycloak server domain, otherwise we default to `https://auth.vseth.ethz.ch`, the `https://` prefix is added, if not present

    Example: \
    `cli-auth -service_account_user foo -service_account_pw bar`


#### Download

- [Linux](https://teamcity.vseth.ethz.ch/repository/download/id0403isg_SipInfrastructure_CliAuth_Production/.lastSuccessful/linux/cli-auth/cli-auth)
- [macOS](https://teamcity.vseth.ethz.ch/repository/download/id0403isg_SipInfrastructure_CliAuth_Production/.lastSuccessful/darwin/cli-auth/cli-auth)

## How it works

Cli-auth first fetches the certificate from the server and generates a random nonce. Then it spawns a webserver locally which it uses to handle the keycloak callback (multiple ports should be provided to avoid conflicts with ports already in use locally). Then the keycloak URL is opened for the user to authenticate. If the user authenticates before a timeout occurs keycloak will redirect to the local callback server with the session token as a fragment in the URL. The callback server servers a javascript program to extract the session token from the fragment and send it to the server as a POST request. The server reads the session token and returns it. The `id_token` is then validated using the certificate and the nonce.

## Build

You can use the `Makefile` to build the binary in a Docker container. See the `Makefile` for optional parameters (e.g. to build for a different platform).
