package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	"gitlab.ethz.ch/vseth/0403-isg/sip-infrastructure/cli-auth/auth"
)

const (
	listSeparator = ","
)

var (
	clientName         = flag.String("client_id", "", "client name")
	rawScope           = flag.String("scope", "", "list of scopes")
	rawPorts           = flag.String("ports", "", "list of ports")
	rawRealm           = flag.String("realm", auth.DefaultRealm, "keycloack realm")
	rawServerDomain    = flag.String("server_domain", auth.DefaultKeycloakDomain, "Domain of the Keycloak server")
	serviceAccountUser = flag.String("service_account_user", "", "Username of the service account to authenticate as")
	serviceAccountPw   = flag.String("service_account_pw", "", "Password of the service account to authenticate as")
	insecure           = flag.Bool("insecure", false, "use http instead of https")
	flowName           = flag.String("flow", "implicit", "the flow that should be used")
)

type authenticator interface {
	Authenticate(scopes ...string) (*auth.Response, error)
}

func main() {
	var a authenticator

	flag.Parse()
	if len(*clientName) > 0 {
		if len(*rawScope) == 0 {
			exitWithError("Please specify the OIDC scopes that you want to request with the -scope flag.")
		} else if len(*rawPorts) == 0 {
			exitWithError("Please set the ports.")
		}
		ports := []int{}
		for _, p := range strings.Split(*rawPorts, listSeparator) {
			port, err := strconv.Atoi(p)
			if err != nil {
				exitWithError("invalid value for argument ports, expects comma seperated integers")
			}
			ports = append(ports, port)
		}

		var flow auth.Flow
		if *flowName == "implicit" {
			flow = auth.ImplicitFlow
		} else if *flowName == "pkce-code" {
			flow = auth.PKCECodeFlow
		} else {
			exitWithError("Invalid flow: \"implicit\" and \"pkce-code\" are supported.")
		}

		a = &auth.Client{
			Flow:           flow,
			Name:           *clientName,
			Ports:          ports,
			KeycloakDomain: *rawServerDomain,
			Realm:          *rawRealm,
			DisableDialog:  true,
			Insecure:       *insecure,
		}
	} else if len(*serviceAccountUser) > 0 {
		// We authenticate using the Client Credentials Grant
		if len(*serviceAccountPw) == 0 {
			exitWithError("Please specify the password of the service account you what to authenticate as")
		}

		a = &auth.ServiceAccountClient{
			Username:       *serviceAccountUser,
			Password:       *serviceAccountPw,
			KeycloakDomain: *rawServerDomain,
			Realm:          *rawRealm,
			Insecure:       *insecure,
		}
	} else {
		exitWithError("Either specify the username and password of the service account you what to authenticate as" +
			" or provide the client name, the requested scopes and set the ports")
	}

	scopes := strings.Split(*rawScope, listSeparator)

	if a == nil {
		exitWithError("initializing authenticator failed - this should never happen")
	}

	resp, err := a.Authenticate(scopes...)
	if err != nil {
		exitWithError(err.Error())
	}
	var b []byte
	if b, err = json.MarshalIndent(resp, "", "  "); err != nil {
		exitWithError(err.Error())
	}
	fmt.Println(string(b))
}

func exitWithError(msg string) {
	fmt.Fprintf(os.Stderr, "{\n  \"error\": \"%s\"\n}\n", msg)
	os.Exit(1)
}
