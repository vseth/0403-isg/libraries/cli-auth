GOOS 				?= $(shell uname -s | tr A-Z a-z)
GOARCH				?= amd64
BINARY_PATH			?= $(PWD)/bin/$(GOOS)
DOCKER_IMAGE_NAME	?= cli-auth-builder

BINARY_NAME = cli-auth
DOCKER_INTERNAL_PATH = /out

default: docker

build_cli: default

docker:
	docker build . -t $(DOCKER_IMAGE_NAME)
	docker run --rm \
		-e GOARCH=$(GOARCH) \
		-e GOOS=$(GOOS) \
		-e OUTPUT=$(DOCKER_INTERNAL_PATH)/$(BINARY_NAME) \
		-v $(BINARY_PATH):/out \
		$(DOCKER_IMAGE_NAME)

# THE FOLLOWING IS ONLY FOR THE NEW TEAMCITY BUILD STEPS!
teamcity_build_proto: ;

teamcity_build_binaries:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./bin/linux/cli-auth ./main.go
	GOOS=windows GOARCH=amd64 go build -o ./bin/windows/cli-auth ./main.go
	GOOS=darwin GOARCH=amd64 go build -o ./bin/darwin/cli-auth ./main.go
	GOOS=darwin GOARCH=arm64 go build -o ./bin/darwin-arm/cli-auth ./main.go