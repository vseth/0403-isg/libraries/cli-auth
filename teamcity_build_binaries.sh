#!/bin/sh

GOOS=linux GOARCH=amd64 go build -o ./bin/linux/cli-auth ./main.go
GOOS=windows GOARCH=amd64 go build -o ./bin/windows/cli-auth ./main.go
GOOS=darwin GOARCH=amd64 go build -o ./bin/darwin/cli-auth ./main.go